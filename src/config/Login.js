import React, {Component} from 'react';
import firebase from 'firebase';
import {Redirect} from 'react-router';
import {Link} from "react-router-dom";

// Initialize Firebase
const config = {
  apiKey: "AIzaSyBlgpLhXELl7zzDmR1UsCExbwvAszBEiOY",
  authDomain: "github-issues-kishan.firebaseapp.com",
  databaseURL: "https://github-issues-kishan.firebaseio.com",
  projectId: "github-issues-kishan",
  storageBucket: "github-issues-kishan.appspot.com",
  messagingSenderId: "530276280834"
};
firebase.initializeApp(config);
var provider = new firebase
  .auth
  .GithubAuthProvider();
provider.addScope('repo');

class Login extends Component {
  constructor(props) {
    super(props)
    sessionStorage.setItem("logged", "false");
  }

  login = () => {
    firebase
      .auth()
      .signInWithPopup(provider)
      .then(function (result) {
        var token = result.credential.accessToken;
        var user = result.user;
        console.log({
          user
        }, {token});
        sessionStorage.setItem("token", `${token}`)
        sessionStorage.setItem("logged", "true")
      })
      .catch(function (error) {
        var errorMessage = error.message;
        console.log({errorMessage});
      });
  }

  renderRedirect = () => {

    if (sessionStorage.getItem("logged") === "true") {
      return <Redirect to='/issues'/>
    }
  }

  render() {
    if (sessionStorage.getItem("logged") === "false") {
      return (
        <div>
          {this.renderRedirect()}
          <Link to="/issues">
            <button id="btn" onClick={this.login}>
              Login
            </button>
          </Link>

        </div>
      )
    }
  }
}

export default Login;