import React, {Component} from 'react';
import './App.css';
import Header from './components/Header';
import Navbar from './components/Navbar';
import Container from './components/Container';
import firebase from 'firebase';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import {
  fetchIssues,
  sortHandler,
  labelHandle,
  authorHandle,
  stateHandle,
  searchHandle
} from './actions/action';

class Main extends Component {

  componentDidMount() {
    this.props.fetchIssues()
  }

  searchHandler = e => {
    this
      .props
      .searchHandle(e, this.props.container)
  }

  labelHandler = label => {
    this
      .props
      .labelHandle(label, this.props.container)
  }

  authorHandler = author => {
    this
      .props
      .authorHandle(author, this.props.container)
  }

  stateHandler = handler => {
    this
      .props
      .stateHandle(handler, this.props.container)
  }

  sortClickHandler = Sort => {
    this
      .props
      .sortHandler(Sort, this.props.container)
  }


  signOut = () => {
    firebase
      .auth()
      .signOut()
      .then(function () {
        sessionStorage.setItem("logged", 'false')
        this.setState({logged: false})
        sessionStorage.clear();
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    // console.log(this.props.container, "featching data")
    if (sessionStorage.getItem("logged") === "false" || !sessionStorage.getItem("logged")) {
      return (<Redirect to="/"/>)
    }
    return (
      <div className="app">
        <button id="logout" onClick={this.signOut}>LogOut</button>
        <Header/>
        <Navbar
          data={this.props.container}
          sort={this.sortClickHandler}
          state={this.stateHandler}
          author={this.authorHandler}
          label={this.labelHandler}
          search={this.searchHandler}
          />
        <Container data={this.props.container} />
      </div>
    );
  }
}

const mapStateToProps = state => ({container: state.reducer.newIssues});

export default connect(mapStateToProps, {
  fetchIssues,
  sortHandler,
  labelHandle,
  authorHandle,
  stateHandle,
  searchHandle
})(Main);
