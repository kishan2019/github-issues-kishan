import React from 'react';
import './menu.css';
const Sort = (props) => {
  const sortItem = ['newest', 'oldest', 'recently updated', 'least recently updated'];

  let arr = sortItem.map((elem, index) => {
    return <option onClick={(e) => props.sortMe(elem)} key={index}>{elem}</option>
  })

  return (
    <div className="dropdown">
      <button className="dropbtn">Sort</button>
      <div className="dropdown-content">
        {arr}
      </div>
    </div>
  )
}
export default Sort;