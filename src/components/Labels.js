import React from 'react';
import './menu.css'

const Labels = (props) => {
  // console.log(props.data)
  let labels = [];
  props
    .data
    .forEach(issue => {
      if (issue.labels.length !== 0) {
        return issue
          .labels
          .reduce((acc, label) => {
            if (acc.includes(label.name)) {
              return acc;
            } else {
              acc.push(label.name);
              return acc;
            }
          }, labels);
      }
    });

  let arr = labels.map((elem, index) => {
    return <option onClick={(e) => props.mylabel(elem)} key={index}>{elem}</option>
  })

  return (
    <div className="dropdown">
      <button className="dropbtn">Labels</button>
      <div className="dropdown-content">
        {arr}
      </div>
    </div>
  )

}
export default Labels;
