import React from 'react';
import './IssuesDetail.css';
import {fetchIssue, fetchComment, deleteSingleComment, addComment} from '../actions/action'
import {connect} from 'react-redux';

class IssuesDetail extends React.Component {

  componentDidMount() {
    this
      .props
      .fetchIssue(this.props.match.params.id)
  }

  deleteComment = id => {
    this
      .props
      .deleteSingleComment(id, this.props.comm)
  }
  addComments = (e) => {
    this
      .props
      .addComment(e, this.props.match.params.id);
  }

  render() {
    return (
      <div className="main-issueDetail">
        <h1>
          {this.props.issu.title}
        </h1>

        <p>
          {this.props.issu.body}
        </p>

        <div className="comment-body">
          <h3>Comments</h3>

          {this
            .props
            .comm
            .map((elem, index) => {
              return (
                <div className="single-comment">
                  <div>{elem['body']}</div>
                  <div>
                    <button id={elem.id} onClick={(e) => this.deleteComment(e)}>
                      Delete
                    </button>
                  </div>
                </div>
              )
            })
}
        </div>
        <div>
          <input
            type="text"
            className="input"
            placeholder="add Comment.."
            onKeyPress={(e) => this.addComments(e)}/>
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => ({issu: state.issuesReducder.newIssues, comm: state.issuesReducder.newComment});
export default connect(mapStateToProps, {fetchIssue, fetchComment, deleteSingleComment, addComment})(IssuesDetail);

// deleteComment = async (event) => {   let id = event.target.id;   const res =
// await
// fetch(`https://api.github.com/repos/kishan2019/movies-rental-api/issues/commen
// ts/${id}`, {     method: "delete",     headers: {       Authorization: `Token
//  ${sessionStorage.getItem("token")}`     }   })
// console.log(this.state.newComment);   if (res.status === 204) {     let
// result = this       .state       .newComment       .filter(comment =>
// comment.id !== parseInt(id))     console.log(result)     this.setState({
// newComment: result })   } } addComments = val => {   if (val.key === "Enter")
// {
// fetch(`https://api.github.com/repos/kishan2019/movies-rental-api/issues/${this
// .props.match.params.id}/comments`, {       method: "POST",       headers: {
//       Authorization: `Token  ${sessionStorage.getItem("token")}`       },
//   body: JSON.stringify({ body: val.target.value })     })
// .then(response => response.json())       .then(data => {         let cmt =
// [...this.state.newComment];         console.log(cmt)         cmt.push(data)
//       return this.setState({ newComment: cmt });       })       .catch(err =>
// console.log(err));     val.target.value = null;   } };
