import React from 'react';
import './Header.css';

function Header(props) {
  let counter = 267;
  return (
    <div>
      <div>
        <div className="header">freeCodeCamp &nbsp; / &nbsp;<b>freeCodeCamp</b>
        </div>
        <div></div>
      </div>
      <div className="issuesNo">issues {counter}</div>
    </div>
  )
}
export default Header;
