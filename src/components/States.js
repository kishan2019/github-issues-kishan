import React from 'react';
import './menu.css';

const States = (props) => {
  
  let state = props.data.reduce((acc, obj) => {
      if (!acc.includes(obj.state)) {
        acc.push(obj.state);
      }
      return acc;
    }, []);
  let arr = state.map((elem, index) => {
    return <option onClick={(e) => props.mystate(elem)} key= { index }> { elem } </option>
  })

  return (
    <div className="dropdown">
      <button className="dropbtn">States</button>
      <div className="dropdown-content">
        {arr}
      </div>
    </div>
  )

}
export default States;
