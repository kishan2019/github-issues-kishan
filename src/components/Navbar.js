import React from 'react';
import Sort from './Sort'
import Searchbar from './Searchbar';
import States from './States';
import Author from './Author';
import Labels from './Labels';
import './navbar.css';

const Navbar = (props) => {
  return (
    <div className="container-nav">
      <Sort sortMe={props.sort} data={props.data} />
      <States mystate={props.state} data={props.data} />
      <Author myauthor={props.author} data={props.data} />
      <Labels mylabel={props.label} data={props.data} />
      <Searchbar search={props.search} data={props.data} />
    </div>
  )
}
export default Navbar;