import React from "react";
import './menu.css'

function Searchbar(props) {
  // console.log(props)
  return (
    <div>
      <input
        type="text"
        className="input"
        placeholder="Search..."
        onKeyDown={(e) => props.search(e, e.target.value)}/>
    </div>
  )
}

export default Searchbar;