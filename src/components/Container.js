import React from 'react';
import './Container.css';
import {Link} from 'react-router-dom'
import moment from 'moment';

const Container = (props) => {
  return props
    .data
    .map((obj, index) => {
      // console.log(obj, "mycurrentvalue");
      return (
        <div className="container-main">
          <div style={{
            margin: "10px"
          }}>
            <i className="material-icons">report</i>
          </div>

          <div className="container">
            <div className="myTitle">
              <Link to={`/issuedetail/${obj.number}`}>
                <div>{obj.title}</div>
              </Link>
              {obj
                .labels
                .map((element, index) => {
                  return (
                    <span
                      style={{
                      backgroundColor: '#' + element.color
                    }}
                      key={index}>{element.name}</span>
                  )
                })}
            </div>

            <div className="title-botom">#{obj.number}
              opened {moment(obj.created_at).fromNow()}
              hours ago by {obj.author_association}
            </div>
          </div>

          <div>
            <i className="material-icons">sms</i>
            {obj.comments}
          </div>
        </div>
      )
    })
}

export default Container;