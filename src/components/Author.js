import React from 'react';
import './menu.css'

const Author = (props) => {
  let author = props
    .data
    .reduce((acc, obj) => {
      if (!acc.includes(obj.user['login'])) {
        acc.push(obj.user['login']);
      }
      return acc;
    }, []);
  let arr = author.map((elem, index) => {
    return <option onClick={(e) => props.myauthor(elem)} key={index}>{elem}</option>
  });

  return (
    <div className="dropdown">
      <button className="dropbtn">Author</button>
      <div className="dropdown-content">
        {arr}
      </div>
    </div>
  )

}
export default Author;
