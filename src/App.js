import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Main from './Main';
import Login from './config/Login'
import IssuesDetail from './components/IssuesDetail';

export class App extends Component {

  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Login}/>
          <Route exact path="/issues" component={Main}/>
          <Route path="/issuedetail/:id" exact component={IssuesDetail}/>
        </Switch>
      </Router>
    )

  }
}

export default App;
