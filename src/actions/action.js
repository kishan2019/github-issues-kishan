export const fetchIssues = () => dispatch => {
  fetch(`https://api.github.com/repos/kishan2019/movies-rental-api/issues?access_token=${sessionStorage.getItem("token")}`)
    .then(res => res.json())
    .then(data => {
      dispatch({type: "FETCH_ISSUES", payload: data});
    })
}

export const fetchIssue = (number) => dispatch => {
  fetch(`https://api.github.com/repos/kishan2019/movies-rental-api/issues/${number}?access_token=${sessionStorage.getItem("token")}`)
    .then(res => res.json())
    .then(data => {
      fetch(`https://api.github.com/repos/kishan2019/movies-rental-api/issues/${number}/comments?access_token=${sessionStorage.getItem("token")}`)
        .then(res => res.json())
        .then(comments => {
          // console.log(data);
          dispatch({
            type: "FETCH_ISSUE",
            payload: {
              data,
              comments
            }
          })
        })
    })
}

export const fetchComment = (number) => dispatch => {
  fetch(`https://api.github.com/repos/kishan2019/movies-rental-api/issues/${number}/comments?access_token=${sessionStorage.getItem("token")}`)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      dispatch({type: "FETCH_cOMMENT", payload: data})
    })
}

export const sortHandler = (sort, data) => dispatch => {
  let temp = [...data];
  let newTemp = [];
  if (sort === 'newest') {
    newTemp = temp.sort((b, a) => {
      return new Date(a.created_at).getTime() - new Date(b.created_at).getTime();
    });
  } else if (sort === 'oldest') {
    newTemp = temp.sort((a, b) => {
      return new Date(a.created_at).getTime() - new Date(b.created_at).getTime();
    });
  } else if (sort === 'recently updated') {
    newTemp = temp.sort((b, a) => {
      return new Date(a.updated_at).getTime() - new Date(b.updated_at).getTime();
    });
  } else if (sort === 'least recently updated') {
    newTemp = temp.sort((a, b) => {
      return new Date(a.updated_at).getTime() - new Date(b.updated_at).getTime();
    });
  }
  dispatch({
    type: "SORT",
    payload: {
      issue: newTemp
    }
  })
};

export const labelHandle = (label, data) => dispatch => {
  let temp = [...data];
  let newTemp = [];
  newTemp = temp.filter(issue => {
    let newIssue = issue
      .labels
      .filter(lab => lab.name === label);
    if (newIssue.length > 0) {
      return true;
    } else {
      return false;
    }
  });
  dispatch({
    type: "LABEL",
    payload: {
      issue: newTemp
    }
  })
}

export const authorHandle = (author, data) => dispatch => {
  let temp = [...data];
  let newTemp = [];
  newTemp = temp.filter(issue => issue.user.login === author);
  dispatch({
    type: "AUTHOR",
    payload: {
      issue: newTemp
    }
  })
}

export const stateHandle = (handler, data) => dispatch => {
  let temp = [...data];
  let newTemp = [];
  newTemp = temp.filter(issue => issue.state === handler);
  dispatch({
    type: "STATE",
    payload: {
      issue: newTemp
    }
  })
}

export const searchHandle = (e, data) => dispatch => {
  if (e.key === 'Enter' && e.target.value.trim() !== '') {
    let temp = [...data];
    let newTemp = [];
    newTemp = temp.filter(issue => issue.title.toLowerCase().includes(e.target.value.toLowerCase()) === true);
    dispatch({
      type: "SEARCH",
      payload: {
        issue: newTemp
      }
    })
    e.target.value = null;
  }
}

export const addComment = (val, number) => dispatch => {

  if (val.key === "Enter") {
    fetch(`https://api.github.com/repos/kishan2019/movies-rental-api/issues/${number}/comments`, {
      method: "POST",
      headers: {
        Authorization: `Token  ${sessionStorage.getItem("token")}`
      },
        body: JSON.stringify({body: val.target.value})
      })
      .then(response => response.json())
      .then(data => {
        dispatch({type: "ADD_COMMENT", payload: data})
      })
      .catch(err => console.log(err));
    val.target.value = null;
  }
};

export const deleteSingleComment = (event, data) => async dispatch => {
  let id = event.target.id;
  const res = await fetch(`https://api.github.com/repos/kishan2019/movies-rental-api/issues/comments/${id}`, {
    method: "delete",
    headers: {
      Authorization: `Token  ${sessionStorage.getItem("token")}`
    }
  })
  if (res.status === 204) {
    let result = data.filter(comment => comment.id !== parseInt(id))
    dispatch({
      type: "DELETE_COMMENT",
      payload: [...result]

    })
  }
}
