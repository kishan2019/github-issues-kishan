const initialState = {
  newIssues: [],
  logged: true
}

const reducer = (state = initialState, action) => {

  switch (action.type) {
    case 'FETCH_ISSUES':
      
        return {
          ...state,
          newIssues: action.payload
        }
      
      
    case 'SORT':
      {
        return {
          ...state,
          newIssues: action.payload.issue
        }
      }
    case 'LABEL':
      {
        return {
          ...state,
          newIssues: action.payload.issue
        }
      }
    case 'AUTHOR':
      {
        return {
          ...state,
          newIssues: action.payload.issue
        }
      }
    case 'STATE':
      {
        return {
          ...state,
          newIssues: action.payload.issue
        }
      }
    case 'SEARCH':
      {
        return {
          ...state,
          newIssues: action.payload.issue
        }
      }
    default:
      {
        return state
      }
  }
}

export default reducer;
