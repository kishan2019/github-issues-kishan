const initialState = {
  newIssues: [],
  newComment: [],
  logged: true
}

const issuesReducer = (state = initialState, action) => {

  switch (action.type) {

    case 'FETCH_ISSUE':
      return {
        ...state,
        newIssues: action.payload.data,
        newComment: action.payload.comments
      }

    case 'ADD_COMMENT':
      let cmt = [...state.newComment]
      cmt.push(action.payload)
      return {
        ...state,
        newComment: cmt
      }

    case 'DELETE_COMMENT':

      return {
        ...state,
        newComment: action.payload
      }

    default:
      return {
        ...state
      }

  }
}

export default issuesReducer;